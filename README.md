# Trabalho Cadastro Funcionários em C

**Professor**: Nivaldi Calonego Junior

**Alunos**
- Zeilker de Matos Rodrigues @zeilkerrodrigues1 (zeilker.rodrigues@unemat.br)
- André Moreira @moreiraandre (andremoreira@unemat.br)

## Diagrama
![diagrama](diagrama.jpg)

## Sobre
Trabalho desenvolvido usando a liguagem de programação _C_ e utilizando
_arquivos TXT_ para salvar e consultar dados.

Foi implementado o seguinte menu de funcionalidades:
- 0 - SAIR
- 1 - NOVO
- 2 - EDITAR
- 3 - RELATÓRIO SALÁRIOS MAIOR QUE A MÉDIA
- 4 - RELATÓRIO ORDENADO POR NOME
- 5 - RELATÓRIO ORDENADO POR SALÁRIO
- 6 - RELATÓRIO IDADES MAIOR QUE A MÉDIA
- 7 - RELATÓRIO RENDA PER-CAPITA

## Clonar repositório
```
git clone https://gitlab.com/moreiraandre/c-arquivo-funcionarios.git
```

## Executar projeto
Executando o arquivo _exec.sh_ serão geradosos arquivos binários e o arquivo 
final. Repare que após a execução do mesmo serão cridos no diretório raiz os 
seguintes arquivos: **principal.o**, **funcoes.o** e **pai**.
```
./exec.sh
```
Não esqueça de entrar no diretório criado após executar o _git clone_ 
(comando: **cd c-arquivo-funcionarios**).

Caso o arquivo esteja sem permissão de _executar_ use o seguinte comando 
**chmod +x exec.sh**

## Arquivos
Após a criação do primeiro registro no cadastro a pasta **DADOS** conterá o 
seguinte arquivo:
- FUNCIONARIOS.DAT

## Referências
- https://homepages.dcc.ufmg.br/~rodolfo/aedsi-2-10/printf_scanf/printfscanf.html
- http://www.cmaismais.com.br/referencia/cstdio/fseek