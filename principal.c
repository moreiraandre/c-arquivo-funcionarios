#include "funcoes/funcao.h"

int main() {
	int op;
	do {
		op = menu();
		switch (op) {
			case 1: novo(); break;
			case 2: editar(); break;
			case 3: relSalariosMaiorMedia(); break;
			case 4: relOrdenado(1); break;
			case 5: relOrdenado(0); break;
			case 6: relIdadesMaiorMedia(); break;
			case 7: relRendaPercapita(); break;
		}
	} while(op != 0);
	return 0;
}
