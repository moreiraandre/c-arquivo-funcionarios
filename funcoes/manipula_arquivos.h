#ifndef manipulaArquivos
#define manipulaArquivos

void abrirArquivos(int somenteLeitura);

void fecharArquivos();

void gravarArquivos();

void lerArquivos();

int getIdxUltimo();

void setIdxEditar(int idx);

int getIdxEditar();

void inserir();

// Setters e Getters
void setNome(char nome[50], int idx);

char *getNome(int idx);

void setIdade(int idade, int idx);

int getIdade(int idx);

void setNFamiliares(int nFamiliares, int idx);

int getNFamiliares(int idx);

void setSalario(int salario, int idx);

int getSalario(int idx);

void setPeso(float peso, int idx);

float getPeso(int idx);

void setAltura(float altura, int idx);

float getAltura(int idx);

#endif
