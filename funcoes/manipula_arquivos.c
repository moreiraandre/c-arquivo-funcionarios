#include <stdio.h> // LEITURA E ESCRITA 
#include <stdlib.h> // system()
#include <string.h> // stricmp()
#include "funcao.h"
#include "manipula_arquivos.h"

// IMPORTANTE!!! O DIRETORIO RAIZ É O DIRETÓRIO DO ARQUIVO FINAL QUE SERÁ EXECUTADO
#define ARQUIVO "DADOS/FUNCIONARIOS.DAT"

FILE *arquivo;

/*==================================
 *|             ESTRUTURA          |
 *==================================*/
struct Funcionario {
    char nome[50];
    int idade, nFamiliares, salario;
    float peso, altura;
};

//       *** FIM ESTRUTURA ***

#define TAM 100
struct Funcionario cadFunc[TAM]; // VETOR QUE ARMAZENARÁ OS REGISTROS DO ARQUIVO

int idxUltimo = -1; // ARMAZENARÁ O ÍNDICE DO ÚLTIMO REGISTRO
int idxEditar = -1; // ARMAZENARÁ O ÍNDICE DO REGISTRO QUE DEVE SER EDITADO NO ARQUIVO

/*===================
 * Getters e Setters
 ====================*/
void setNome(char nome[50], int idx) {
    strcpy(cadFunc[idx].nome, nome);
}

char *getNome(int idx) {
    return cadFunc[idx].nome;
}

void setIdade(int idade, int idx) {
    cadFunc[idx].idade = idade;
}

int getIdade(int idx) {
    return cadFunc[idx].idade;
}

void setNFamiliares(int nFamiliares, int idx) {
    cadFunc[idx].nFamiliares = nFamiliares;
}

int getNFamiliares(int idx) {
    return cadFunc[idx].nFamiliares;
}

void setSalario(int salario, int idx) {
    cadFunc[idx].salario = salario;
}

int getSalario(int idx) {
    return cadFunc[idx].salario;
}

void setPeso(float peso, int idx) {
    cadFunc[idx].peso = peso;
}

float getPeso(int idx) {
    return cadFunc[idx].peso;
}

void setAltura(float altura, int idx) {
    cadFunc[idx].altura = altura;
}

float getAltura(int idx) {
    return cadFunc[idx].altura;
}

int getIdxUltimo() {
    return idxUltimo; // RETORNA O ÍNDICE DO ÚLTIMO REGISTRO
}

void setIdxEditar(int idx) {
    idxEditar = idx;
}

int getIdxEditar() {
    return idxEditar;
}
// *** Fim Getters e Setters ***

void inserir() {
    idxUltimo++; // CRIA UMA NOVA LINHA (LÓGICA) PARA O CADASTRO
}


void abrirArquivos(int somenteLeitura) {
    arquivo = fopen(ARQUIVO, "ab"); // GARANTE A CRIAÇÃO FÍSICA DO ARQUIVO, SOMENTE GRAVA REGISTROSNO FIM DO ARQUIVO (DESCONSIDERA POSIOCIONAMENTO)

    fecharArquivos();
    if (somenteLeitura == 1)
        arquivo = fopen(ARQUIVO, "rb"); // ABRE O ARQUIVO BINÁRIO EM MODO SOMENTE LEITURA
    else if (somenteLeitura == 0)
        arquivo = fopen(ARQUIVO, "r+b"); // ABRE O ARQUIVO BINÁRIO EM MODO LEITURA E ESCRITA (CONSIDERA POSICIONAMENTO)
    else if (somenteLeitura == 2)
        arquivo = fopen(ARQUIVO, "ab"); // ABRE O ARQUIVO BINÁRIO EM MODO LEITURA E ESCRITA (CONSIDERA POSICIONAMENTO)

    if (arquivo == NULL)
        system("read -p \"Não foi possível abrir o arquivo!\" saindo");
}

void fecharArquivos() {
    fclose(arquivo);
}


void gravarArquivos() {
    abrirArquivos(0);

    if (getIdxEditar() != -1) {
        fseek(arquivo, sizeof(struct Funcionario) * getIdxEditar(),
              SEEK_SET); // POSICIONA O PONTEIRO NO REGISTRO DESEJADO
        fwrite(&cadFunc[getIdxEditar()], sizeof(struct Funcionario), 1, arquivo); // SALVA O REGISTRO NO ARQUIVO
    } else {
        fecharArquivos();
        abrirArquivos(2); // ABRIR NO MODO DE ADIÇÃO DE REGISTROS NO FIM DO ARQUIVO
        fwrite(&cadFunc[getIdxUltimo()], sizeof(struct Funcionario), 1, arquivo); // SALVA O REGISTRO NO ARQUIVO
    }

    setIdxEditar(-1); // INDICANDO QUE NÃO EXISTE REGISTRO PENDENTE DE EDIÇÃO

    fecharArquivos();
}

// LÊ OS DADOS DOS ARQUIVOS E OS ARMAZENA NOS VETORES
void lerArquivos() {
    int x = 0;
    abrirArquivos(1);
    while (!feof(arquivo)) {
        fread(&cadFunc[x], sizeof(struct Funcionario), 1, arquivo);
        x++;
    }
    idxUltimo = x - 2; // INDICANDO A ÚLTIMA LINHA
    fecharArquivos();
}
