#include <stdio.h> // LEITURA E ESCRITA 
#include <stdlib.h> // system()
#include <string.h> // stricmp()
#include "funcao.h"
#include "manipula_arquivos.h"

int menu() {
    system("clear"); // LIMPA A TELA *ANTES* DE EXIBIR O MENU

    lerArquivos(); // CARRREGA OS DADOS DO ARQUIVO NO VETOR

    int op;
    printf("========================================\n");
    printf("|                  MENU                |\n");
    printf("========================================\n");
    printf("0 - SAIR\n");
    printf("1 - NOVO\n");
    printf("2 - EDITAR\n");
    printf("3 - RELATÓRIO SALÁRIOS MAIOR QUE A MÉDIA\n");
    printf("4 - RELATÓRIO ORDENADO POR NOME\n");
    printf("5 - RELATÓRIO ORDENADO POR SALÁRIO\n");
    printf("6 - RELATÓRIO IDADES MAIOR QUE A MÉDIA\n");
    printf("7 - RELATÓRIO RENDA PER-CAPITA\n");
    printf("\nINFORME A OPÇÃO DESEJADA E PRESSIONE <ENTER>: ");
    scanf("%i", &op);
    system("clear"); // LIMPA A TELA *DEPOIS* DE EXIBIR O MENU
    return op;
}

/*
 * Esta função é responsável por ler os dados do teclado, associalos ao vetor do cadastro e salvar os dados nos arquivos
 *
 * O PARAMETRO idxCadastro INDICA QUAL O ÍNDICE DO CADASTRO QUE RECEBERÁ OS DADOS
 */
void lerTeclado(int idxCadastro) {
    char nome[50];
    int
            idade,
            nFamiliares,
            salario;
    float
            peso,
            altura;

    printf("INFORME OS SEGUINTES DADOS:\n");
    printf("---------------------------\n");
    printf("NOME: ");
    // ESSE 'BENDITO' ESPAÇO DEPOIS DA PRIMEIRA ASPA É IMPORTANTE PARA O FUNCIONAMENTO, ELE LIMPA O BUFFER ANTES DE COMEÇAR A LER.
    // ESSA FORMATAÇÃO DE TIPO %[^\n]s É NECESSÁRIO PARA QUE A LEITURA SEJA FEITA ATÉ A QUEBRA DE LINHA AO INVÉS DO PRIMEIRO ESPAÇO.
    scanf(" %[^\n]s", nome);
    setNome(nome, idxCadastro);
    printf("IDADE: ");
    scanf("%i", &idade);
    setIdade(idade, idxCadastro);
    printf("N. DE FAMILIARES: ");
    scanf("%i", &nFamiliares);
    setNFamiliares(nFamiliares, idxCadastro);
    printf("SALARIO: ");
    scanf("%i", &salario);
    setSalario(salario, idxCadastro);
    printf("PESO: ");
    scanf("%f", &peso);
    setPeso(peso, idxCadastro);
    printf("ALTURA: ");
    scanf("%f", &altura);
    setAltura(altura, idxCadastro);

    gravarArquivos();
}

void novo() {
    inserir(); // INCREMENTA A VARIÁVEL CONTROLADORA DO ÚLTIMO REGISTRO
    lerTeclado(getIdxUltimo());
    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}

void editar() {
    char nomeBusca[50];
    int
            x = 0,
            idxEncontrou = -1;
            
    if (getIdxUltimo() > -1) {
        printf("======================== FUNÇÃO EDITAR =======================\n");
        printf("==============================================================\n");
        printf("INFORME O NOME PARA BUSCA: ");
        scanf(" %[^\n]s", nomeBusca);

        while ((x <= getIdxUltimo()) && idxEncontrou == -1) {
            if (strcmp(getNome(x), nomeBusca) == 0)
                idxEncontrou = x;

            x++;
        }

        if (idxEncontrou != -1) {
            printf("                  DADOS ATUAIS\n");
            printf("--------------------------------------------\n");
            printf("NOME: %s\n", getNome(idxEncontrou));
            printf("IDADE: %d\n", getIdade(idxEncontrou));
            printf("N. DE FAMILIARES: %d\n", getNFamiliares(idxEncontrou));
            printf("SALARIO: %d\n", getSalario(idxEncontrou));
            printf("PESO: %f\n", getPeso(idxEncontrou));
            printf("ALTURA: %f\n", getAltura(idxEncontrou));

            printf("           ATUALIZANDO O CADASTRO\n");
            printf("--------------------------------------------\n");
            setIdxEditar(idxEncontrou); // INFORMA A FUNÇÃO gravarArquivos() QUAL REGISTRO DEVE EDITAR
            lerTeclado(idxEncontrou);
        } else
            printf("BUSCA NÃO ENCONTRADA!\n");
    } else
        printf("NÃO HÁ DADOS!\n\n");

    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}

void relSalariosMaiorMedia() {
    int
            media = 0,
            idx;

    // CALCULANDO MÉDIA GERAL DE SALÁRIO
    if (getIdxUltimo() > -1) {
        for (idx = 0; idx <= getIdxUltimo(); idx++)
            media += getSalario(idx); // SOMA TODOS OS SALÁRIOS

        media = media / idx; // DIVIDE A SOMA PELA QUANTIDADE DE SALÁRIOS

        printf("************* RELATÓRIO SALÁRIOS MAIOR QUE A MÉDIA ***********\n\n");
        printf("                                              NOME  SALÁRIO R$\n");
        printf("==============================================================\n");
        for (idx = 0; idx <= getIdxUltimo(); idx++) {
            if (getSalario(idx) > media) {
                printf("%50s", getNome(idx));
                printf("  %-11i\n", getSalario(idx));
            }
        }
    } else
        printf("NÃO HÁ DADOS!\n\n");
    
    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}

// TROCA OS DADOS DA POSIÇÃO idx1 COM idx2
void trocaPosicoes(int idx1, int idx2) {
    int idx;

    char nome_aux[50];
    int
            idade_aux,
            nFamiliares_aux,
            salario_aux;

    float
            peso_aux,
            altura_aux;

    strcpy(nome_aux, getNome(idx1));
    idade_aux = getIdade(idx1);
    nFamiliares_aux = getNFamiliares(idx1);
    salario_aux = getSalario(idx1);
    peso_aux = getPeso(idx1);
    altura_aux = getAltura(idx1);

    setNome(getNome(idx2), idx1);
    setIdade(getIdade(idx2), idx1);
    setNFamiliares(getNFamiliares(idx2), idx1);
    setSalario(getSalario(idx2), idx1);
    setPeso(getPeso(idx2), idx1);
    setAltura(getAltura(idx2), idx1);

    setNome(nome_aux, idx2);
    setIdade(idade_aux, idx2);
    setNFamiliares(nFamiliares_aux, idx2);
    setSalario(salario_aux, idx2);
    setPeso(peso_aux, idx2);
    setAltura(altura_aux, idx2);
}

void relOrdenado(int ordenaNome) {
    int idx = 0, x, y;

    // CONDIÇÕES PARA TROCAR AS POSIÇÕES
    if (getIdxUltimo() > -1) {
        for (x = 0; x <= getIdxUltimo() - 1; x++) {
            for (y = x + 1; y <= getIdxUltimo(); y++) {
                if (ordenaNome == 1) {
                    if (getNome(x) > getNome(y))
                        trocaPosicoes(x, y);
                } else {
                    if (getSalario(x) > getSalario(y))
                        trocaPosicoes(x, y);
                }
            }
        }

        if (ordenaNome == 1)
            printf("***************** RELATÓRIO ORDENADO POR NOME ****************\n\n");
        else
            printf("*************** RELATÓRIO ORDENADO POR SALÁRIO ***************\n\n");

        printf("                                              NOME  SALÁRIO R$\n");
        printf("==============================================================\n");
        for (idx = 0; idx <= getIdxUltimo(); idx++) {
            printf("%50s", getNome(idx));
            printf("  %-11i\n", getSalario(idx));
        }
    } else
        printf("NÃO HÁ DADOS!\n\n");
        
    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}

void relIdadesMaiorMedia() {
    int media = 0, idx = 1;

    // CALCULANDO MÉDIA GERAL DE IDADE 
    if (getIdxUltimo() > -1) {
        for (idx = 0; idx <= getIdxUltimo(); idx++)
            media += getIdade(idx); // SOMA TODAS AS IDADES
        media = media / idx;

        printf("****************** RELATÓRIO IDADES MAIOR QUE A MÉDIA ***************\n\n");
        printf("                                              NOME  IDADE  SALÁRIO R$\n");
        printf("=====================================================================\n");
        for (idx = 0; idx <= getIdxUltimo(); idx++) {
            if (getIdade(idx) > media) {
                printf("%50s", getNome(idx));
                printf("  %i", getIdade(idx));
                printf("     %-11i\n", getSalario(idx));
            }
        }
    } else
        printf("NÃO HÁ DADOS!\n\n");

    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}

void relRendaPercapita() {
    int idx;
    
    if (getIdxUltimo() > -1) {
	    printf("******************** RELATÓRIO RENDA PER-CAPITA ********************\n\n");
	    printf("                                              NOME  RENDA PER-CAPITA\n");
	    printf("====================================================================\n");

        for (idx = 0; idx <= getIdxUltimo(); idx++) {
            printf("%50s", getNome(idx));
            printf("  %-11i\n", getSalario(idx) / getNFamiliares(idx));
        }
    } else
        printf("NÃO HÁ DADOS!\n\n");

    system("read -p \"PRESSIONE <ENTER> PARA RETORNAR AO MENU\" saindo");
}
