#include <stdio.h> // LEITURA E ESCRITA 
#include <stdlib.h> // system()
#include <string.h> // stricmp()
#include "funcao.h"
#include "manipula_arquivos.h"

// IMPORTANTE!!! O DIRETORIO RAIZ É O DIRETÓRIO DO ARQUIVO FINAL QUE SERÁ EXECUTADO
#define ARQUIVO_NOMES "DADOS/NOMES.TXT"
#define ARQUIVO_IDADES "DADOS/IDADES.TXT"
#define ARQUIVO_NFAMILIARES "DADOS/NFAMILIARES.TXT"
#define ARQUIVO_SALARIOS "DADOS/SALARIOS.TXT"
#define ARQUIVO_PESOS "DADOS/PESOS.TXT"
#define ARQUIVO_ALTURAS "DADOS/ALTURAS.TXT"

FILE
        *arquivo_nomes,
        *arquivo_idades,
        *arquivo_nfamiliares,
        *arquivo_salarios,
        *arquivo_pesos,
        *arquivo_alturas;

/*==================================
 *             VETORES             |
 *==================================*/
#define TAMANHO_VETOR 100

char vnome[TAMANHO_VETOR][50];

int
        vidade[TAMANHO_VETOR],
        vnFamiliares[TAMANHO_VETOR],
        vsalario[TAMANHO_VETOR];

float
        vpeso[TAMANHO_VETOR],
        valtura[TAMANHO_VETOR];
//       *** FIM VETORES ***

int idxUltimo = -1; // ARMAZENARÁ O ÍNDICE DO ÚLTIMO REGISTRO

/*===================
 * Getters e Setters
 ====================*/
void setNome(char nome[50], int idx) {
    strcpy(vnome[idx], nome);
}

char *getNome(int idx) {
    return vnome[idx];
}

void setIdade(int idade, int idx) {
    vidade[idx] = idade;
}

int getIdade(int idx) {
    return vidade[idx];
}

void setNFamiliares(int nFamiliares, int idx) {
    vnFamiliares[idx] = nFamiliares;
}

int getNFamiliares(int idx) {
    return vnFamiliares[idx];
}

void setSalario(int salario, int idx) {
    vsalario[idx] = salario;
}

int getSalario(int idx) {
    return vsalario[idx];
}

void setPeso(float peso, int idx) {
    vpeso[idx] = peso;
}

float getPeso(int idx) {
    return vpeso[idx];
}

void setAltura(float altura, int idx) {
    valtura[idx] = altura;
}

float getAltura(int idx) {
    return valtura[idx];
}

int getIdxUltimo() {
    return idxUltimo; // RETORNA O ÍNDICE DO ÚLTIMO REGISTRO
}
// *** Fim Getters e Setters ***

void inserir() {
    idxUltimo++; // CRIA UMA NOVA LINHA (LÓGICA) PARA O CADASTRO
}

void abrirArquivos(int somenteLeitura) {
    // ESSE BLOCO GARANTE QUE OS ARQUIVOS SERÃO CRIADOS CASO NÃO EXISTAM
    arquivo_nomes = fopen(ARQUIVO_NOMES, "a");
    arquivo_idades = fopen(ARQUIVO_IDADES, "a");
    arquivo_nfamiliares = fopen(ARQUIVO_NFAMILIARES, "a");
    arquivo_salarios = fopen(ARQUIVO_SALARIOS, "a");
    arquivo_pesos = fopen(ARQUIVO_PESOS, "a");
    arquivo_alturas = fopen(ARQUIVO_ALTURAS, "a");
    //******************************************************************

    fecharArquivos();
    if (somenteLeitura) {
        arquivo_nomes = fopen(ARQUIVO_NOMES, "r");
        arquivo_idades = fopen(ARQUIVO_IDADES, "r");
        arquivo_nfamiliares = fopen(ARQUIVO_NFAMILIARES, "r");
        arquivo_salarios = fopen(ARQUIVO_SALARIOS, "r");
        arquivo_pesos = fopen(ARQUIVO_PESOS, "r");
        arquivo_alturas = fopen(ARQUIVO_ALTURAS, "r");
    } else {
        arquivo_nomes = fopen(ARQUIVO_NOMES, "w");
        arquivo_idades = fopen(ARQUIVO_IDADES, "w");
        arquivo_nfamiliares = fopen(ARQUIVO_NFAMILIARES, "w");
        arquivo_salarios = fopen(ARQUIVO_SALARIOS, "w");
        arquivo_pesos = fopen(ARQUIVO_PESOS, "w");
        arquivo_alturas = fopen(ARQUIVO_ALTURAS, "w");
    }

    if (
            (arquivo_nomes == NULL)
            || (arquivo_idades == NULL)
            || (arquivo_nfamiliares == NULL)
            || (arquivo_salarios == NULL)
            || (arquivo_pesos == NULL)
            || (arquivo_alturas == NULL)
            ) {
        system("read -p \"Não foi possível abrir o arquivo!\" saindo");
    }
}

void fecharArquivos() {
    fclose(arquivo_nomes);
    fclose(arquivo_idades);
    fclose(arquivo_nfamiliares);
    fclose(arquivo_salarios);
    fclose(arquivo_pesos);
    fclose(arquivo_alturas);
}


void gravarArquivos() {
    int x;

    abrirArquivos(0);

    for (x = 0; x <= getIdxUltimo(); x++) {
        fprintf(arquivo_nomes, "%s\n", vnome[x]);
        fprintf(arquivo_idades, "%d\n", vidade[x]);
        fprintf(arquivo_nfamiliares, "%d\n", vnFamiliares[x]);
        fprintf(arquivo_salarios, "%d\n", vsalario[x]);
        fprintf(arquivo_pesos, "%f\n", vpeso[x]);
        fprintf(arquivo_alturas, "%f\n", valtura[x]);
    }

    fecharArquivos();
}

// LÊ OS DADOS DOS ARQUIVOS E OS ARMAZENA NOS VETORES
void lerArquivos() {
    int x = 0;
    abrirArquivos(1);
    while (!feof(arquivo_nomes)) {
        fscanf(arquivo_nomes, " %[^\n]s", vnome[x]);
        fscanf(arquivo_idades, "%d", &vidade[x]);
        fscanf(arquivo_nfamiliares, "%d", &vnFamiliares[x]);
        fscanf(arquivo_salarios, "%d", &vsalario[x]);
        fscanf(arquivo_pesos, "%f", &vpeso[x]);
        fscanf(arquivo_alturas, "%f", &valtura[x]);

        x++;
    }
    /*
     * A ÚLTIMA LINHA ESTÁ EM BRANCO, DEPOIS DE LÊ-LA O CONTADOR É INCREMENTADO ANTES DE ENCONTRAR O FIM DO ARQUIVO.
     * POR ISSO A NECESSIDADE DE RETORNAR DUAS POSIÇÕES
     */
    idxUltimo = x - 2;

    fecharArquivos();
}
